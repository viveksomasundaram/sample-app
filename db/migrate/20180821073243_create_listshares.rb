class CreateListshares < ActiveRecord::Migration[5.1]
  def change
    create_table :listshares do |t|
      t.integer :list_id
      t.integer :user_id

      t.timestamps
    end
    add_index :listshares, [:list_id, :user_id]
  end
end
