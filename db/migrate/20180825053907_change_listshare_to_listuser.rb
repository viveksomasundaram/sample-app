class ChangeListshareToListuser < ActiveRecord::Migration[5.1]
  def change
    rename_table :listshares, :listusers
  end
end
