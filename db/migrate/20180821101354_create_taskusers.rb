class CreateTaskusers < ActiveRecord::Migration[5.1]
  def change
    create_table :taskusers do |t|
      t.integer :task_id
      t.integer :user_id
      t.integer :list_id
      t.timestamps
    end
    add_index :taskusers, [:task_id, :user_id]
  end
end
