class User < ApplicationRecord
  has_many :lists, dependent: :destroy
  has_many :tasks,dependent: :destroy
  has_many :listusers, through: :lists
  has_many :taskusers, dependent: :destroy
  before_save { self.email = email.downcase }
  validates :name,  presence: true, length: { maximum: 50 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
  has_secure_password
  validates :password, presence: true, length: { minimum: 6 }
  
  
  scope :all_except, ->(user) { where.not(id: user) }
  

  def all_lists
    List.where("user_id = ? or id = (?)", self.id, self.listusers.pluck(:list_id))
  end
# Review: what is this method for? Looks like duplicate of all_lists
# Review: can we use has_many through here
  def collaborated_lists
    List.where("id IN (?)", self.listusers.pluck(:list_id) )
  end
# Review: can we use has_many through here
  def assigned_tasks
    Task.where("id IN (?)", self.taskusers.pluck(:task_id) )
  end


  
  
end
