class List < ApplicationRecord
  has_many :tasks, dependent: :destroy
  has_many :listusers, dependent: :destroy
  has_many :users, through: :listusers
  has_many :taskusers, dependent: :destroy
  belongs_to :user
  default_scope -> { order(created_at: :desc) }
  validates :user_id, presence: true
  validates :content, presence: true, length: { maximum: 140 }

  
end
