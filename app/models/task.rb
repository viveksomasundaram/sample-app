class Task < ApplicationRecord
  belongs_to :list
  has_many :taskusers, dependent: :destroy
  has_many :users, through: :taskusers
  default_scope -> { order(created_at: :desc) }
  validates :name, presence: true
  validates :content, presence: true, length: { maximum: 140 }
end
