class Taskuser < ApplicationRecord
    belongs_to :user
    belongs_to :task
    belongs_to :list
# Review: scope by list_id too
    validates_uniqueness_of :user_id, :scope => :task_id, :scope => :list_id , :message => " Already Assigned"

end
