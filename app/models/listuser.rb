class Listuser < ApplicationRecord
    belongs_to :user
    belongs_to :list
    validates_uniqueness_of :user_id, :scope => :list_id , :message => " Already shared"
    before_destroy :remove_assigned_task

    private 
# This should be a model callback, in before_destroy
    def remove_assigned_task
        # Review: Can we use list.task_users.where(user_id: self.user_id).destroy_all
        @taskid = list.task_users.where(user_id: self.user_id).destroy_all
        # Read about delete_all vs destroy_all   
    end
end
