class ListusersController < ApplicationController
    before_action :logged_in_user, only: [:create, :destroy]
    before_action :load_listuser, only: [:destroy]
    def create
        @list_share = Listuser.new(list_share_params) 
        if @list_share.save
            flash[:success] = "List shared"
            redirect_to root_url
        else
            flash[:warning] = @list_share.errors.full_messages.to_sentence
            redirect_to root_url
        end
    end

    def destroy
        if @listuser.destroy_all
            flash[:success] = "User deleted"
            redirect_to root_url
        end
    end
    
    private

    def list_share_params
        params.require(:listuser).permit(:user_id,:list_id)
    end

    def load_listuser
        # We need to scope this by current_user
        @listuser = Listuser.where("id = ? And list_id IN (?)",params[:id],current_user.lists.pluck(:id))
    end
    
end
