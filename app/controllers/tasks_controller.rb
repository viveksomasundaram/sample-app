class TasksController < ApplicationController

    def create
        # Review: Need to check if user has access to list
        @task = Task.new(task_params) 
        if @task.save
            flash[:success] = "Task created!"
            redirect_to :controller => 'lists', :action => 'show' , id: params[:task][:list_id]
        else
            flash[:success] = @task.errors.full_messages.to_sentence
            redirect_to :controller => 'lists', :action => 'show' , id: params[:task][:list_id]
        end
    end

    private

    def task_params
        params.require(:task).permit(:name,:content,:list_id)
    end
end
