class StaticPagesController < ApplicationController
  def home
    if logged_in?
      @users = User.all
      @lists = current_user.lists
    end
  end
  
  

  def help
  end
  def about
  end
  def contact
  end
end
