class TaskusersController < ApplicationController
    before_action :logged_in_user, only: [:create, :destroy]
    def create
        # Review: Need to check if user has access to task
        @task_user = Taskuser.new(task_share_params) 
        if @task_user.save
            flash[:success] = "Task Assigned"
            redirect_to request.referrer
        else
            flash[:warning] = @task_user.errors.full_messages.to_sentence
            redirect_to request.referrer
        end
    end

    private

    def task_share_params
        params.require(:taskusers).permit(:user_id,:task_id,:list_id)
    end
end
