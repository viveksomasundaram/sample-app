class ListsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]
  before_action :load_list, only: [:show]

  def home
    @collaborators = User.all_except(current_user).collect {|p| [ p.email, p.id ] }
    @user_lists = current_user.lists
    @collaborated_lists = current_user.collaborated_lists
    @assigned_tasks = current_user.assigned_tasks
  end

  def create
    @list = current_user.lists.new(list_params) 
    if @list.save
      flash[:success] = "List created!"
      redirect_to action: 'show', id: @list.id
    else
      flash[:danger] = @list.errors.full_messages.to_sentence
      redirect_to root_url
    end
  end

  def show
    @tasks = @list.tasks
    @collaborated_users = @list.users.collect {|p| [ p.email, p.id ] }
  end

  private

  def list_params
    params.require(:list).permit(:name, :content)
  end

  def load_list
    @list = current_user.all_lists.where(id: params[:id]).first
    return redirect_to root_url if @list.nil?
  end
end