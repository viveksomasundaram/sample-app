Rails.application.routes.draw do

  get 'password_resets/new'

  get 'password_resets/edit'

  get 'sessions/new'

  root 'lists#home'
  get  '/help',    to: 'static_pages#help'
  get  '/about',   to: 'static_pages#about'
  get  '/contact', to: 'static_pages#contact'
  get  '/signup', to: 'users#new'
  post '/signup',  to: 'users#create'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  resources :users do
    member do
    get :following, :followers
    end
  end 
  # Review: Can we use lists/new & tasks/new pages here?
  get  '/list/new',  to: 'lists#new'
  get  '/lists/:id',  to: 'lists#show'
  # Review: Nest tasks/new under lists
  get  '/task/:id',  to: 'tasks#new'

  # Review: Can we limit the allowed actions for ALL routes?
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  resources :microposts,          only: [:create, :destroy]
  resources :relationships,       only: [:create, :destroy]
  resources :lists,                 only: [:new, :create, :destroy, :edit]
  # Review: Can we nest tasks, list_share, taskusers inside lists
  # Review: Can we use underscored names for listusers, taskusers i.e changing them to list_users and taks_users 
  resources :tasks,          only: [:create, :destroy]
  resources :listusers,        only: [:create, :destroy]
  # Review: Can we nest taskusers inside tasks
  resources :taskusers,        only: [:create, :destroy]


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
